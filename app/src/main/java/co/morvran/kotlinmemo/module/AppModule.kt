package co.morvran.kotlinmemo.module

import android.app.Application
import android.content.Context
import co.morvran.kotlinmemo.helpers.MemosDatabaseHelper
import co.morvran.kotlinmemo.memos.dao.MemoDAO
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    fun provideContext(application: Application): Context {
        return application
    }

    @Singleton
    @Provides
    fun provideNoteDAO(applicationContext: Context): MemoDAO {
        return MemosDatabaseHelper.getDatabase(applicationContext).memoDAO()
    }
}