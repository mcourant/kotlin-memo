package co.morvran.kotlinmemo.helpers

import android.content.Context
import androidx.room.Room
import co.morvran.kotlinmemo.memos.MemosDatabase

class MemosDatabaseHelper(context: Context?) {
    companion object {
        private var dbHelper: MemosDatabaseHelper? = null
        private lateinit var db: MemosDatabase

        @Synchronized
        fun getDatabase(c: Context): MemosDatabase {
            if (dbHelper == null)
                dbHelper =
                    MemosDatabaseHelper(
                        c.applicationContext
                    )

            return db
        }
    }

    init {
        db = context?.let {
            Room
                .databaseBuilder(it, MemosDatabase::class.java, "memos.db")
                .allowMainThreadQueries()
                .build()
        }!!
    }
}