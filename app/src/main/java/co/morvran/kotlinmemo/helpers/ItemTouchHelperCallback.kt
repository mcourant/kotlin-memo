package co.morvran.kotlinmemo.helpers

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import co.morvran.kotlinmemo.memos.adapters.MemoAdapter

class ItemTouchHelperCallback(adapter: MemoAdapter) : ItemTouchHelper.Callback() {

    private val memoAdapter: MemoAdapter = adapter

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        memoAdapter.onItemDismiss(viewHolder)
        memoAdapter.notifyDataSetChanged()
    }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
        val dragFlagsRight = RIGHT
        return makeMovementFlags(
            dragFlagsRight,
            dragFlagsRight
        )
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        target: ViewHolder
    ): Boolean {
        return false
    }

}