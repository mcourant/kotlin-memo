package co.morvran.kotlinmemo.repositories


import android.util.Log
import co.morvran.kotlinmemo.di.DIApplication
import co.morvran.kotlinmemo.memos.dao.MemoDAO
import co.morvran.kotlinmemo.memos.dto.MemoDTO
import javax.inject.Inject

class MainRepository {
    @Inject lateinit var memoDAO: MemoDAO

    fun insertMemo(memo: MemoDTO) {
        memoDAO.insert(memo)
    }

    fun getAllMemos(): List<MemoDTO> {
        return memoDAO.getListMemo()
    }

    fun deleteNote(memo: MemoDTO) {
        memoDAO.delete(memo)
    }

    init {
        DIApplication.getAppComponent()?.inject(this)
    }
}