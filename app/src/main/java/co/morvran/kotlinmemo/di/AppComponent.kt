package co.morvran.kotlinmemo.di

import android.app.Application
import co.morvran.kotlinmemo.module.AppModule
import co.morvran.kotlinmemo.repositories.MainRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(mainRepository: MainRepository)
}