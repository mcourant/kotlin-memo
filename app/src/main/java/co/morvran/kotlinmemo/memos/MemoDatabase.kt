package co.morvran.kotlinmemo.memos

import androidx.room.Database
import androidx.room.RoomDatabase
import co.morvran.kotlinmemo.memos.dao.MemoDAO
import co.morvran.kotlinmemo.memos.dto.MemoDTO

@Database(entities = [MemoDTO::class], version = 1)
abstract class MemosDatabase : RoomDatabase() {
    abstract fun memoDAO(): MemoDAO
}