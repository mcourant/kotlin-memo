package co.morvran.kotlinmemo.memos.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "memos")
class MemoDTO (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    var message: String = "") {

    constructor(message: String) : this() {
        this.message = message
    }
}