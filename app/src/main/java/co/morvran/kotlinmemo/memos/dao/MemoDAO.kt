package co.morvran.kotlinmemo.memos.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import co.morvran.kotlinmemo.memos.dto.MemoDTO

@Dao
abstract class MemoDAO {
    @Query("SELECT * FROM memos")
    abstract fun getListMemo(): MutableList<MemoDTO>

    @Insert
    abstract fun insert(vararg memos: MemoDTO?)

    @Update
    abstract fun update(vararg memos: MemoDTO?)

    @Delete
    abstract fun delete(vararg memos: MemoDTO?)
}