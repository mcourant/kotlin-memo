package co.morvran.kotlinmemo.memos.adapters

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.morvran.kotlinmemo.R
import co.morvran.kotlinmemo.activities.DetailActivity
import co.morvran.kotlinmemo.fragments.DetailFragment
import co.morvran.kotlinmemo.helpers.MemosDatabaseHelper
import co.morvran.kotlinmemo.memos.dto.MemoDTO
import kotlinx.android.synthetic.main.memo_item.view.*

class MemoAdapter(listMemo: MutableList<MemoDTO>, main: AppCompatActivity) : RecyclerView.Adapter<MemoAdapter.MemoViewHolder?>() {
    
    private var listMemo: MutableList<MemoDTO> = ArrayList()
    private val main: AppCompatActivity
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val viewMemo: View =
            LayoutInflater.from(parent.context).inflate(R.layout.memo_item, parent, false)
        return MemoViewHolder(viewMemo)
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        holder.bind(listMemo[position])
    }

    override fun getItemCount(): Int {
        return listMemo.size
    }

    fun onItemDismiss(view: RecyclerView.ViewHolder) {
        if (view.adapterPosition > -1) {
            MemosDatabaseHelper.getDatabase(view.itemView.context).memoDAO().delete(listMemo[view.adapterPosition])
            listMemo.removeAt(view.adapterPosition)
            notifyItemRemoved(view.adapterPosition)
        }
    }

    inner class MemoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { view ->
                val memo: MemoDTO = listMemo[adapterPosition]
                val preferences: SharedPreferences =
                    androidx.preference.PreferenceManager.getDefaultSharedPreferences(view.context)
                val editor = preferences.edit()
                editor.putInt("last", adapterPosition)
                editor.apply()

                if (main.findViewById<View?>(R.id.details) == null) {
                    val intent = Intent(view.context, DetailActivity::class.java)
                    intent.putExtra("detail", memo.message)
                    view.context.startActivity(intent)
                } else {
                    val fragment = DetailFragment()
                    val bundle = Bundle()
                    bundle.putString(DetailFragment.EXTRA_PARAM, memo.message)
                    fragment.arguments = bundle

                    val fragmentManager =
                        main.supportFragmentManager

                    val fragmentTransaction =
                        fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.details, fragment, "detailsActivity")
                    fragmentTransaction.commit()
                }
            }
        }

        fun bind(memo:MemoDTO) = with(itemView){
            titleMemo.text = memo.message
        }
    }

    init {
        this.listMemo = listMemo
        this.main = main
    }
}