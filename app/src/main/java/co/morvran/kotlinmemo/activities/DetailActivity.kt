package co.morvran.kotlinmemo.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity;
import co.morvran.kotlinmemo.R
import co.morvran.kotlinmemo.fragments.DetailFragment
import kotlinx.android.synthetic.main.fragment_detail.view.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val message = intent.getStringExtra("detail")

        val fragment = DetailFragment()
        val bundle = Bundle()
        bundle.putString(DetailFragment.EXTRA_PARAM, message)
        fragment.arguments = bundle

        val fragmentManager = supportFragmentManager
        val fragmentTransaction =
            fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.details, fragment, "detailsActivity")
        fragmentTransaction.commit()
    }

    fun afficheNote(v: View) {
        Toast.makeText(this, v.txt.text, Toast.LENGTH_SHORT).show()
    }
}