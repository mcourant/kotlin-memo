package co.morvran.kotlinmemo.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import co.morvran.kotlinmemo.R
import co.morvran.kotlinmemo.helpers.ItemTouchHelperCallback
import co.morvran.kotlinmemo.memos.adapters.MemoAdapter
import co.morvran.kotlinmemo.memos.dto.MemoDTO
import co.morvran.kotlinmemo.repositories.MainRepository
import co.morvran.kotlinmemo.viewmodel.ViewModel

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: ViewModel;
    private lateinit var memoAdapter: MemoAdapter;
    private lateinit var itemTouchHelper: ItemTouchHelper;
    private var listMemo: MutableList<MemoDTO>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        viewModel.init(MainRepository())
        listMemo = viewModel.getMemo()?.toMutableList()

        memoAdapter = MemoAdapter(listMemo!!, this)

        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(this)
        itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(memoAdapter))
        itemTouchHelper.attachToRecyclerView(recycler_view)
        recycler_view.adapter = memoAdapter
    }

    fun insertMemo(v: View) {
        if (addNote.text.toString().isNotEmpty()) {
            val n = MemoDTO(addNote!!.text.toString())
            listMemo?.add(n)
            viewModel.insertMemo(n)
            recycler_view.adapter?.notifyDataSetChanged()
        }
        addNote.text.clear()
    }
}
