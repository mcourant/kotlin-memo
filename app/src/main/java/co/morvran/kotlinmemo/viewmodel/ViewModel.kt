package co.morvran.kotlinmemo.viewmodel

import androidx.lifecycle.ViewModel
import co.morvran.kotlinmemo.memos.dto.MemoDTO
import co.morvran.kotlinmemo.repositories.MainRepository

class ViewModel : ViewModel() {

    private lateinit var mainRepository: MainRepository

    fun init(mainRepository: MainRepository) {
        this.mainRepository = mainRepository
    }

    fun insertMemo(memo: MemoDTO) {
        mainRepository.insertMemo(memo)
    }

    fun getMemo(): List<MemoDTO>? {
        return mainRepository.getAllMemos()
    }
}